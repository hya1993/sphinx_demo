﻿using System;
using System.Windows.Forms;

namespace Sphinx_demo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.ExitProgram = new System.Windows.Forms.Button();
            this.Go = new System.Windows.Forms.Button();
            this.FullScreen = new System.Windows.Forms.Button();
            this.Exit = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.BackToMainPage = new System.Windows.Forms.Button();
            this.mainMenu1 = new System.Windows.Forms.MainMenu(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.homePageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.urlInput = new System.Windows.Forms.RichTextBox();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.timer3 = new System.Windows.Forms.Timer(this.components);
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.UrlStr = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.Confirm = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.LGID = new System.Windows.Forms.TextBox();
            this.LoginPW = new System.Windows.Forms.TextBox();
            this.LoginUN = new System.Windows.Forms.TextBox();
            this.UNID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.PWID = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.YesRadio = new System.Windows.Forms.RadioButton();
            this.NoRadio = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.RefreshFreq = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.RetrievalPanel = new System.Windows.Forms.TableLayoutPanel();
            this.RetrievalConfirm = new System.Windows.Forms.Button();
            this.RetrieveIDInput = new System.Windows.Forms.TextBox();
            this.RetrieveID = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.timer4 = new System.Windows.Forms.Timer(this.components);
            this.menuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.RetrievalPanel.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // ExitProgram
            // 
            this.ExitProgram.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ExitProgram.Location = new System.Drawing.Point(378, 878);
            this.ExitProgram.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.ExitProgram.Name = "ExitProgram";
            this.ExitProgram.Size = new System.Drawing.Size(122, 35);
            this.ExitProgram.TabIndex = 43;
            this.ExitProgram.Text = "Exit Program";
            this.ExitProgram.UseVisualStyleBackColor = true;
            this.ExitProgram.Click += new System.EventHandler(this.ExitProgram_Click);
            // 
            // Go
            // 
            this.Go.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Go.Location = new System.Drawing.Point(191, 878);
            this.Go.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Go.Name = "Go";
            this.Go.Size = new System.Drawing.Size(56, 34);
            this.Go.TabIndex = 41;
            this.Go.Text = "Go";
            this.Go.UseVisualStyleBackColor = true;
            this.Go.Click += new System.EventHandler(this.Go_Click);
            // 
            // FullScreen
            // 
            this.FullScreen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.FullScreen.Location = new System.Drawing.Point(764, 364);
            this.FullScreen.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.FullScreen.Name = "FullScreen";
            this.FullScreen.Size = new System.Drawing.Size(152, 35);
            this.FullScreen.TabIndex = 38;
            this.FullScreen.Text = "Full Screen";
            this.FullScreen.UseVisualStyleBackColor = true;
            this.FullScreen.Click += new System.EventHandler(this.FullScreen_Click);
            // 
            // Exit
            // 
            this.Exit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Exit.Location = new System.Drawing.Point(560, 364);
            this.Exit.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Exit.Name = "Exit";
            this.Exit.Size = new System.Drawing.Size(173, 35);
            this.Exit.TabIndex = 37;
            this.Exit.Text = "Exit Full Screen";
            this.Exit.UseVisualStyleBackColor = true;
            this.Exit.Click += new System.EventHandler(this.Exit_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 300;
            // 
            // BackToMainPage
            // 
            this.BackToMainPage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BackToMainPage.Location = new System.Drawing.Point(254, 878);
            this.BackToMainPage.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.BackToMainPage.Name = "BackToMainPage";
            this.BackToMainPage.Size = new System.Drawing.Size(118, 34);
            this.BackToMainPage.TabIndex = 42;
            this.BackToMainPage.Text = "Main Page";
            this.BackToMainPage.UseVisualStyleBackColor = true;
            this.BackToMainPage.Click += new System.EventHandler(this.BackToMainPage_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(154)))), ((int)(((byte)(221)))));
            this.menuStrip1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("menuStrip1.BackgroundImage")));
            this.menuStrip1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.homePageToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(980, 62);
            this.menuStrip1.TabIndex = 45;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // homePageToolStripMenuItem
            // 
            this.homePageToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.homePageToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.homePageToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI Black", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.homePageToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.homePageToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("homePageToolStripMenuItem.Image")));
            this.homePageToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.homePageToolStripMenuItem.Name = "homePageToolStripMenuItem";
            this.homePageToolStripMenuItem.Padding = new System.Windows.Forms.Padding(15, 0, 4, 0);
            this.homePageToolStripMenuItem.Size = new System.Drawing.Size(325, 58);
            this.homePageToolStripMenuItem.Text = "     Home Page";
            this.homePageToolStripMenuItem.Click += new System.EventHandler(this.homePageToolStripMenuItem_Click);
            // 
            // urlInput
            // 
            this.urlInput.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.urlInput.Location = new System.Drawing.Point(12, 876);
            this.urlInput.Name = "urlInput";
            this.urlInput.Size = new System.Drawing.Size(160, 37);
            this.urlInput.TabIndex = 46;
            this.urlInput.Text = "";
            // 
            // timer2
            // 
            this.timer2.Enabled = true;
            this.timer2.Interval = 3000;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // timer3
            // 
            this.timer3.Interval = 3000000;
            this.timer3.Tick += new System.EventHandler(this.timer3_Tick);
            // 
            // webBrowser1
            // 
            this.webBrowser1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser1.Location = new System.Drawing.Point(0, 0);
            this.webBrowser1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(22, 22);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.ScriptErrorsSuppressed = true;
            this.webBrowser1.Size = new System.Drawing.Size(980, 925);
            this.webBrowser1.TabIndex = 44;
            this.webBrowser1.Url = new System.Uri("https://bipreporting.flextronics.com/BOE/OpenDocument/opendoc/openDocument.jsp?sI" +
        "DType=CUID&iDocID=AcCUQsqE6pxIjgB1srhXgs0", System.UriKind.Absolute);
            this.webBrowser1.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.webBrowser1_DocumentCompleted);
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.UrlStr);
            this.panel1.Controls.Add(this.tableLayoutPanel1);
            this.panel1.Controls.Add(this.tableLayoutPanel2);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.RefreshFreq);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Location = new System.Drawing.Point(18, 235);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(980, 644);
            this.panel1.TabIndex = 47;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(250, 12);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(356, 36);
            this.label10.TabIndex = 17;
            this.label10.Text = "Default Setting for this PC";
            // 
            // UrlStr
            // 
            this.UrlStr.Location = new System.Drawing.Point(256, 62);
            this.UrlStr.Name = "UrlStr";
            this.UrlStr.Size = new System.Drawing.Size(399, 28);
            this.UrlStr.TabIndex = 11;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 47.45223F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 52.54777F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 181F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 182F));
            this.tableLayoutPanel1.Controls.Add(this.Confirm, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.LGID, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.LoginPW, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.LoginUN, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.UNID, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.FullScreen, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.Exit, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.PWID, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label7, 2, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 199);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 65F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 61F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(920, 402);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // Confirm
            // 
            this.Confirm.Location = new System.Drawing.Point(740, 129);
            this.Confirm.Name = "Confirm";
            this.Confirm.Size = new System.Drawing.Size(173, 28);
            this.Confirm.TabIndex = 12;
            this.Confirm.Text = "Save";
            this.Confirm.UseVisualStyleBackColor = true;
            this.Confirm.Click += new System.EventHandler(this.Confirm_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 65);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(249, 22);
            this.label8.TabIndex = 17;
            this.label8.Text = "HTMLElementID for password";
            // 
            // LGID
            // 
            this.LGID.Location = new System.Drawing.Point(267, 129);
            this.LGID.Name = "LGID";
            this.LGID.Size = new System.Drawing.Size(158, 28);
            this.LGID.TabIndex = 15;
            // 
            // LoginPW
            // 
            this.LoginPW.Location = new System.Drawing.Point(740, 68);
            this.LoginPW.Name = "LoginPW";
            this.LoginPW.PasswordChar = '*';
            this.LoginPW.Size = new System.Drawing.Size(170, 28);
            this.LoginPW.TabIndex = 14;
            // 
            // LoginUN
            // 
            this.LoginUN.Location = new System.Drawing.Point(740, 3);
            this.LoginUN.Name = "LoginUN";
            this.LoginUN.Size = new System.Drawing.Size(170, 28);
            this.LoginUN.TabIndex = 12;
            // 
            // UNID
            // 
            this.UNID.Location = new System.Drawing.Point(267, 3);
            this.UNID.Name = "UNID";
            this.UNID.Size = new System.Drawing.Size(158, 28);
            this.UNID.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(559, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(144, 22);
            this.label1.TabIndex = 9;
            this.label1.Text = "Login UserName";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(257, 22);
            this.label3.TabIndex = 2;
            this.label3.Text = "HTMLElementID for UserName";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 126);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(215, 44);
            this.label5.TabIndex = 4;
            this.label5.Text = "HTMLElementID for login button";
            // 
            // PWID
            // 
            this.PWID.Location = new System.Drawing.Point(267, 68);
            this.PWID.Name = "PWID";
            this.PWID.Size = new System.Drawing.Size(158, 28);
            this.PWID.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(559, 65);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(138, 22);
            this.label7.TabIndex = 10;
            this.label7.Text = "Login Password";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 71.25891F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28.74109F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tableLayoutPanel2.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.YesRadio, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.NoRadio, 2, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(7, 149);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(648, 34);
            this.tableLayoutPanel2.TabIndex = 6;
            this.tableLayoutPanel2.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel2_Paint);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(309, 22);
            this.label2.TabIndex = 1;
            this.label2.Text = "Do you want to auto login to this site?";
            // 
            // YesRadio
            // 
            this.YesRadio.AutoSize = true;
            this.YesRadio.Location = new System.Drawing.Point(350, 3);
            this.YesRadio.Name = "YesRadio";
            this.YesRadio.Size = new System.Drawing.Size(66, 26);
            this.YesRadio.TabIndex = 2;
            this.YesRadio.TabStop = true;
            this.YesRadio.Text = "Yes";
            this.YesRadio.UseVisualStyleBackColor = true;
            // 
            // NoRadio
            // 
            this.NoRadio.AutoSize = true;
            this.NoRadio.Location = new System.Drawing.Point(490, 3);
            this.NoRadio.Name = "NoRadio";
            this.NoRadio.Size = new System.Drawing.Size(58, 26);
            this.NoRadio.TabIndex = 3;
            this.NoRadio.TabStop = true;
            this.NoRadio.Text = "No";
            this.NoRadio.UseVisualStyleBackColor = true;
            this.NoRadio.CheckedChanged += new System.EventHandler(this.NoRadio_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 62);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(215, 22);
            this.label4.TabIndex = 11;
            this.label4.Text = "Please input your url here";
            // 
            // RefreshFreq
            // 
            this.RefreshFreq.Location = new System.Drawing.Point(256, 106);
            this.RefreshFreq.Name = "RefreshFreq";
            this.RefreshFreq.Size = new System.Drawing.Size(399, 28);
            this.RefreshFreq.TabIndex = 16;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 107);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(227, 22);
            this.label6.TabIndex = 5;
            this.label6.Text = "Refresh frequency(millisec)";
            // 
            // RetrievalPanel
            // 
            this.RetrievalPanel.BackColor = System.Drawing.Color.White;
            this.RetrievalPanel.ColumnCount = 3;
            this.RetrievalPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32.38866F));
            this.RetrievalPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34.0081F));
            this.RetrievalPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.40081F));
            this.RetrievalPanel.Controls.Add(this.RetrievalConfirm, 2, 0);
            this.RetrievalPanel.Controls.Add(this.RetrieveIDInput, 1, 0);
            this.RetrievalPanel.Controls.Add(this.RetrieveID, 0, 0);
            this.RetrievalPanel.Location = new System.Drawing.Point(18, 52);
            this.RetrievalPanel.Name = "RetrievalPanel";
            this.RetrievalPanel.RowCount = 2;
            this.RetrievalPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.RetrievalPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.RetrievalPanel.Size = new System.Drawing.Size(488, 55);
            this.RetrievalPanel.TabIndex = 48;
            // 
            // RetrievalConfirm
            // 
            this.RetrievalConfirm.Location = new System.Drawing.Point(327, 3);
            this.RetrievalConfirm.Name = "RetrievalConfirm";
            this.RetrievalConfirm.Size = new System.Drawing.Size(143, 28);
            this.RetrievalConfirm.TabIndex = 1;
            this.RetrievalConfirm.Text = "Confirm";
            this.RetrievalConfirm.UseVisualStyleBackColor = true;
            this.RetrievalConfirm.Click += new System.EventHandler(this.RetrievalConfirm_Click);
            // 
            // RetrieveIDInput
            // 
            this.RetrieveIDInput.Location = new System.Drawing.Point(161, 3);
            this.RetrieveIDInput.Name = "RetrieveIDInput";
            this.RetrieveIDInput.Size = new System.Drawing.Size(139, 28);
            this.RetrieveIDInput.TabIndex = 0;
            // 
            // RetrieveID
            // 
            this.RetrieveID.AutoSize = true;
            this.RetrieveID.Location = new System.Drawing.Point(3, 0);
            this.RetrieveID.Name = "RetrieveID";
            this.RetrieveID.Size = new System.Drawing.Size(94, 22);
            this.RetrieveID.TabIndex = 2;
            this.RetrieveID.Text = "RetrieveID";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.RetrievalPanel);
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Controls.Add(this.textBox2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 62);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(980, 863);
            this.panel2.TabIndex = 49;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(21, 17);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(144, 22);
            this.label9.TabIndex = 3;
            this.label9.Text = "Admin Password";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(179, 15);
            this.textBox2.Name = "textBox2";
            this.textBox2.PasswordChar = '*';
            this.textBox2.Size = new System.Drawing.Size(139, 28);
            this.textBox2.TabIndex = 49;
            this.textBox2.TextChanged += new System.EventHandler(this.textBox2_TextChanged_1);
            // 
            // timer4
            // 
            this.timer4.Tick += new System.EventHandler(this.timer4_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(980, 925);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.urlInput);
            this.Controls.Add(this.BackToMainPage);
            this.Controls.Add(this.ExitProgram);
            this.Controls.Add(this.Go);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.webBrowser1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Menu = this.mainMenu1;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.RetrievalPanel.ResumeLayout(false);
            this.RetrievalPanel.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ExitProgram;
        private System.Windows.Forms.Button Go;
        private System.Windows.Forms.Button FullScreen;
        private System.Windows.Forms.Button Exit;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button BackToMainPage;
        private System.Windows.Forms.MainMenu mainMenu1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.RichTextBox urlInput;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.ToolStripMenuItem homePageToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Timer timer3;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.TextBox UrlStr;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.TextBox LGID;
        public System.Windows.Forms.TextBox LoginPW;
        public System.Windows.Forms.TextBox LoginUN;
        public System.Windows.Forms.TextBox UNID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.TextBox PWID;
        private System.Windows.Forms.Label label7;
        public System.Windows.Forms.TextBox RefreshFreq;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.RadioButton YesRadio;
        public System.Windows.Forms.RadioButton NoRadio;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button Confirm;
        private System.Windows.Forms.TableLayoutPanel RetrievalPanel;
        private System.Windows.Forms.Button RetrievalConfirm;
        private System.Windows.Forms.TextBox RetrieveIDInput;
        private System.Windows.Forms.Label RetrieveID;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox textBox2;
        private Label label9;
        private Label label10;
        private Timer timer4;
    }
}

