﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Management;
using System.Runtime.InteropServices;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Net;
using System.Net.Sockets;
using System.Data.OleDb;
using System.Data.SqlClient;


//solutin_configuration:new x64, debug->build x64, app.config:x64 Upload proj:change x86 to 64
//https://ourcodeworld.com/articles/read/173/how-to-use-cefsharp-chromium-embedded-framework-csharp-in-a-winforms-application
//Install-Package CefSharp.WinForms

namespace Sphinx_demo
{
    // Hook shortcut
    public partial class Form1 : Form, IMessageFilter
    {

        Keyboard kb = new Keyboard();
        string MyIp;
        string ConnectionStrSQL = "Data Source=AURNT145;Initial Catalog=p_npi-paperless_Enclosures;User Id=npiweb;Password=np1w3b;";
        int ID;
        //string ConnectionStrSQL = "Provider=sqloledb;Data Source=AURNT145;Initial Catalog=p_npi-paperless_Enclosures;User Id=npiweb;Password=np1w3b;";
        //DataSet dst = null;
        //OleDbConnection con = default(OleDbConnection);
        //OleDbCommand cmdUpdate = default(OleDbCommand);
        //OleDbDataAdapter dad;
        List<string> privateIP = new List<string>();
        bool NewletterIP;
        string urlString = "https://flextronics365.sharepoint.com/sites/business_technology_solutions_applications/SitePages/Home.aspx";
        bool autoLogin;
        string usernameID;
        string passwordID;
        string submitID;
        string loginUsername;
        string loginPassword;
        int refreshFrequency = 20;
        bool is_sec_page = false;
        bool webbrowserDocumentCompleted = false;
        public Form1()
        {            

            kb.HookStart();
            FormBorderStyle = FormBorderStyle.None;
            WindowState = FormWindowState.Maximized;
            InitializeComponent();
            //Newsletter.BackColor = Color.FromArgb(200, Newsletter.BackColor);
            //Support.BackColor = Color.FromArgb(200, Support.BackColor);
            //Continued.BackColor = Color.FromArgb(200, Continued.BackColor);
            //EventSchedule.BackColor = Color.FromArgb(200, EventSchedule.BackColor);
            Application.AddMessageFilter(this);    
            PowerHelper.ForceSystemAwake();
            Startup Autostart = new Startup();
            Autostart.InstallMeOnStartUp();
            textBox2.Hide();
            ExitProgram.Hide();
            urlInput.Hide();
            Go.Hide();
            BackToMainPage.Hide();
            Exit.Hide();
            FullScreen.Hide();
            TopMost = true;
            urlInput.Text = "https://";
            webBrowser1.Show();
            //MessageBox.Show(Environment.MachineName);
            panel2.Hide();
            timer4.Interval = 2000;
            //MyIp = GetIP();
            MyIp = Environment.MachineName;
            //dst = new DataSet();
            //con = new OleDbConnection(ConnectionStrSQL);
            //webBrowser1.Hide();
            webBrowser1.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(webBrowser1_DocumentCompleted);
            //webBrowser1.DocumentCompleted += (o, e) =>
            //{
            //    webbrowserDocumentCompleted = true;
            //};
            //DataSet ds = new DataSet();SELECT COUNT(*) from users where user_name like 'Adam' AND password like '123456'
            int recordNum = 0;
            string ipQuery = "select count(*) from SphinxLogin where IPAddress = '" + MyIp + "'";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStrSQL))
                {
                    using (SqlCommand comm = new SqlCommand(ipQuery, conn))
                    {
                        conn.Open();
                        recordNum = (int)comm.ExecuteScalar();
                        comm.ExecuteNonQuery();
                    }
                }
            } catch(Exception er)
            {
                Console.WriteLine(er.Message);
            }

            string insertQuery = insertQuery = @"INSERT INTO SphinxLog (machineName, time)
VALUES ('" + MyIp + "', '" + DateTime.Now + "');";
            
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStrSQL))
                {
                    using (SqlCommand comm = new SqlCommand(insertQuery, conn))
                    {
                        conn.Open();
                        comm.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            if (recordNum >= 1)
            {
                using (var connection = new SqlConnection(ConnectionStrSQL))
                {
                    connection.Open();
                    string ip = "SELECT * FROM SphinxLogin WHERE IPAddress='" + MyIp + "';";
                    using (var cmd = new SqlCommand(ip, connection))
                    {
                        SqlDataReader re = cmd.ExecuteReader();
                        //string urlString;
                        //bool autoLogin;
                        //string usernameID;
                        //string passwordID;
                        //string submitID;
                        //string loginUsername;
                        //string loginPassword;
                        //int refreshFrequency;
                        if (re.Read())
                        {
                            urlString = re["Url"].ToString(); // only fills using first product in table
                            autoLogin = (bool)re["AutoLogin"];
                            usernameID = re["UserNameID"] == null ? null : re["UserNameID"].ToString();
                            passwordID = re["PasswordID"] == null ? null : re["PasswordID"].ToString();
                            submitID = re["SubmitButtonID"] == null ? null : re["SubmitButtonID"].ToString();
                            loginUsername = re["UserName"] == null ? null : re["UserName"].ToString();
                            if (re["Password"] == null)
                            {
                                loginPassword = null;
                            } else
                            {
                                loginPassword = Crypter.Decrypt(re["Password"].ToString());
                            }
                            
                            bool res = int.TryParse(re["RefreshFrequency"].ToString(), out refreshFrequency);
                            if (!res)
                            {
                                timer3.Stop();
                            } else
                            {
                                timer3.Start();
                                    timer3.Interval = refreshFrequency;
                                var refres = timer3.Interval;
                                
                            }
                        }

                    }
                }
                webBrowser1.Navigate(urlString);
                if (autoLogin)
                {
                    //await for page to load, timeout 10 seconds.
                    //your code will run after the page loaded or timeout.

                    try
                    {

                        HtmlDocument doc = webBrowser1.Document;
                        HtmlElement username = doc.GetElementById(usernameID);
                        HtmlElement password = doc.GetElementById(passwordID);
                        HtmlElement submit = doc.GetElementById(submitID);
                        username.SetAttribute("value", loginUsername);
                        password.SetAttribute("value", loginPassword);
                        if (submit != null)
                        {
                            submit.InvokeMember("click");
                        }
                    }
                    catch (Exception err)
                    {
                        Console.WriteLine(err.Message);
                    }
                }

                else
                {

                }
                
            }
            //using (con = new  OleDbConnection(ConnectionStrSQL))
            //using (cmdUpdate = new OleDbCommand(query, con))
            //using (dad = new OleDbDataAdapter(cmdUpdate))
            //{
            //    dad.Fill(ds);
            //}
            //DataTable dt = ds.Tables[0];
            //for (var i = 0; i < dt.Rows.Count - 1; i++)
            //{
            //    privateIP.Add(dt.Rows[i]["IPAddress"].ToString());
            //}
            //NewletterIP = !privateIP.Contains(MyIp);
            //if (NewletterIP)
            //{
            //    webBrowser1.Url = new System.Uri("http://aurntpas/Newsletter/Newsletter/LatestVolume?Sphinx=True");
            //    webBrowser1.Show();
            //    tableLayoutPanel1.Hide();
            //}
            //else
            //{
            //    webBrowser1.Hide();
            //}


        }


        protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);
        }

      

        private void Form1_Load(object sender, EventArgs e)
        {
            IntPtr intPtr = Process.GetCurrentProcess().MainWindowHandle;
            EdgeGestureUtil.DisableEdgeGestures(intPtr, true);

            //float scaleX = ((float)Screen.PrimaryScreen.WorkingArea.Width / 1920);
            //float scaleY = ((float)Screen.PrimaryScreen.WorkingArea.Height / 1080);
            //SizeF aSf = new SizeF(scaleX, scaleY);
            //this.Scale(aSf);

        }

        // Shortcut: alt+F->full screem; alt+A->Admin
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            const int WM_KEYDOWN = 0x100;
            const int WM_SYSKEYDOWN = 0x104;

            if ((msg.Msg == WM_KEYDOWN) || (msg.Msg == WM_SYSKEYDOWN))
            {
                switch (keyData)
                {
                    case Keys.Control | Keys.S:
                        panel2.Show();
                        panel1.Hide();
                        RetrievalPanel.Hide();
                        textBox2.Show();   
                        break;
                    case Keys.Control | Keys.H:
                        Exit.Hide();
                        FullScreen.Hide();
                        textBox2.Hide();
                        Go.Hide();
                        urlInput.Hide();
                        ExitProgram.Hide();
                        BackToMainPage.Hide();
                        panel2.Hide();

                        break;
                    case Keys.Control | Keys.U:
                        Exit.Show();
                        FullScreen.Show();
                        panel2.Show();
                        panel1.Show();
                        RetrievalPanel.Hide();
                        break;
                }
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

      

        // Admin mode 
        // Input url
        private void Go_Click(object sender, EventArgs e)
        {
            string url = urlInput.Text;
            if (url == "http://" || url == "")
            {
                webBrowser1.Url = new System.Uri("http://aurntpas/Newsletter/Newsletter/LatestVolume?Sphinx=True");
            } else
            {
                webBrowser1.Url = new System.Uri(url);
            }
            //HideMainPageIcon();
            webBrowser1.Show();
           
        }

        // Back to Newsletter
        private void BackToMainPage_Click(object sender, EventArgs e)
        {
            webBrowser1.Url = new System.Uri(urlString);      
        }

        // Full screen
        private void FullScreen_Click(object sender, EventArgs e)
        {
            FormBorderStyle = FormBorderStyle.None;
            WindowState = FormWindowState.Maximized;
        }
        
        // Exit full screen
        private void Exit_Click(object sender, EventArgs e)
        {
            FormBorderStyle = FormBorderStyle.Sizable;
            WindowState = FormWindowState.Normal;

        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            // Disable right click
            Application.RemoveMessageFilter(this);
        }

       
        private void ExitProgram_Click(object sender, EventArgs e)
        {
            Application.Exit();
            PowerHelper.ResetSystemDefault();
        }

        
        // Password textbox
        public void textBox2_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void urlInput_TextChanged(object sender, EventArgs e)
        {
            if (urlInput.Text.ToString().Length > 200)
            {
                MessageBox.Show("The URL exceed the length limit");
            }
        }

        // Disable zoom in/out
        internal static int HIWORD(IntPtr wParam)
        {
            return (int)((wParam.ToInt64() >> 16) & 0xffff);
        }

        internal static int LOWORD(IntPtr wParam)
        {
            return (int)(wParam.ToInt64() & 0xffff);
        }
        [Flags]
        internal enum VIRTUAL_KEY_STATES
        {
            NONE = 0x0000,
            LBUTTON = 0x0001,
            RBUTTON = 0x0002,
            SHIFT = 0x0004,
            CTRL = 0x0008,
            MBUTTON = 0x0010,
            XBUTTON1 = 0x0020,
            XBUTTON2 = 0x0040
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct POINT
        {
            public int X;
            public int Y;

            public POINT(int x, int y)
            {
                X = x;
                Y = y;
            }
            public POINT(Point pt)
            {
                X = pt.X;
                Y = pt.Y;
            }
            public Point ToPoint()
            {
                return new Point(X, Y);
            }
            public void AssignTo(ref Point destination)
            {
                destination.X = X;
                destination.Y = Y;
            }
            public void CopyFrom(Point source)
            {
                X = source.X;
                Y = source.Y;
            }
            public void CopyFrom(POINT source)
            {
                X = source.X;
                Y = source.Y;
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct RECT
        {
            public int Left;
            public int Top;
            public int Right;
            public int Bottom;

            public RECT(Rectangle source)
            {
                Left = source.Left;
                Top = source.Top;
                Right = source.Right;
                Bottom = source.Bottom;
            }
            public RECT(int x, int y, int width, int height)
            {
                Left = x;
                Top = y;
                Right = Left + width;
                Bottom = Top + height;
            }
            public int Width
            {
                get { return Right - Left; }
            }
            public int Height
            {
                get { return Bottom - Top; }
            }
            public Rectangle ToRectangle()
            {
                return new Rectangle(Left, Top, Width, Height);
            }
            public void Inflate(int dx, int dy)
            {
                Left -= dx;
                Top -= dy;
                Right += dx;
                Bottom += dy;
            }
            public void Deflate(int leftMargin, int topMargin, int rightMargin, int bottomMargin)
            {
                Left += leftMargin;
                Top += topMargin;
                Right -= rightMargin;
                Bottom -= bottomMargin;
                if (Bottom < Top)
                {
                    Bottom = Top;
                }
                if (Right < Left)
                {
                    Right = Left;
                }
            }
            public void Offset(int dx, int dy)
            {
                Left += dx;
                Top += dy;
                Right += dx;
                Bottom += dy;
            }
        }

        [Flags]
        internal enum TOUCH_FLAGS
        {
            NONE = 0x00000000
        }

        [Flags]
        internal enum TOUCH_MASK
        {
            NONE = 0x00000000,
            CONTACTAREA = 0x00000001,
            ORIENTATION = 0x00000002,
            PRESSURE = 0x00000004,
        }

        internal enum POINTER_INPUT_TYPE
        {
            POINTER = 0x00000001,
            TOUCH = 0x00000002,
            PEN = 0x00000003,
            MOUSE = 0x00000004
        }
        [Flags]
        internal enum POINTER_FLAGS
        {
            NONE = 0x00000000,
            NEW = 0x00000001,
            INRANGE = 0x00000002,
            INCONTACT = 0x00000004,
            FIRSTBUTTON = 0x00000010,
            SECONDBUTTON = 0x00000020,
            THIRDBUTTON = 0x00000040,
            FOURTHBUTTON = 0x00000080,
            FIFTHBUTTON = 0x00000100,
            PRIMARY = 0x00002000,
            CONFIDENCE = 0x00004000,
            CANCELED = 0x00008000,
            DOWN = 0x00010000,
            UPDATE = 0x00020000,
            UP = 0x00040000,
            WHEEL = 0x00080000,
            HWHEEL = 0x00100000,
            CAPTURECHANGED = 0x00200000,
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct POINTER_TOUCH_INFO
        {
            [MarshalAs(UnmanagedType.Struct)]
            public POINTER_INFO PointerInfo;
            public TOUCH_FLAGS TouchFlags;
            public TOUCH_MASK TouchMask;
            [MarshalAs(UnmanagedType.Struct)]
            public RECT ContactArea;
            [MarshalAs(UnmanagedType.Struct)]
            public RECT ContactAreaRaw;
            public uint Orientation;
            public uint Pressure;
        }


        [StructLayout(LayoutKind.Sequential)]
        internal struct POINTER_INFO
        {
            public POINTER_INPUT_TYPE pointerType;
            public int PointerID;
            public int FrameID;
            public POINTER_FLAGS PointerFlags;
            public IntPtr SourceDevice;
            public IntPtr WindowTarget;
            [MarshalAs(UnmanagedType.Struct)]
            public POINT PtPixelLocation;
            [MarshalAs(UnmanagedType.Struct)]
            public POINT PtPixelLocationRaw;
            [MarshalAs(UnmanagedType.Struct)]
            public POINT PtHimetricLocation;
            [MarshalAs(UnmanagedType.Struct)]
            public POINT PtHimetricLocationRaw;
            public uint Time;
            public uint HistoryCount;
            public uint InputData;
            public VIRTUAL_KEY_STATES KeyStates;
            public long PerformanceCount;
            public int ButtonChangeType;
        }

        internal const int
       WM_PARENTNOTIFY = 0x0210,
       WM_NCPOINTERUPDATE = 0x0241,
       WM_NCPOINTERDOWN = 0x0242,
       WM_NCPOINTERUP = 0x0243,
       WM_POINTERUPDATE = 0x0245,
       WM_POINTERDOWN = 0x0246,
       WM_POINTERUP = 0x0247,
       WM_POINTERENTER = 0x0249,
       WM_POINTERLEAVE = 0x024A,
       WM_POINTERACTIVATE = 0x024B,
       WM_POINTERCAPTURECHANGED = 0x024C,
       WM_POINTERWHEEL = 0x024E,
       WM_POINTERHWHEEL = 0x024F,

       // WM_POINTERACTIVATE return codes
       PA_ACTIVATE = 1,
       PA_NOACTIVATE = 3,

       MAX_TOUCH_COUNT = 256;

        private void timer4_Tick(object sender, EventArgs e)
        {
            MessageBox.Show("hi");
            //try
            //{

            //    HtmlDocument doc = webBrowser1.Document;
            //    HtmlElement username = doc.GetElementById(usernameID);
            //    HtmlElement password = doc.GetElementById(passwordID);
            //    //if (username == null || password == null)
            //    //{
            //    //    MessageBox.Show("hi");
            //    //    timer4.Stop();
            //    //    timer4.Start();
            //    //}

            //        HtmlElement submit = doc.GetElementById(submitID);
            //    username.SetAttribute("value", loginUsername);
            //    password.SetAttribute("value", loginPassword);
            //    if (submit != null)
            //    {
            //        submit.InvokeMember("click");
            //    }
            //}
            //catch (Exception err)
            //{
            //    Console.WriteLine(err.Message);
            //}
            //timer4.Stop();
        }

        private void tableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void textBox2_TextChanged_1(object sender, EventArgs e)
        {
            if (textBox2.Text.ToString().IndexOf("admin.123") >= 0)
            {
                panel2.Show();
                RetrievalPanel.Show();
                if (urlInput.Visible)
                {
                    panel1.Show();
                    ExitProgram.Hide();
                    urlInput.Hide();
                    Go.Hide();
                    BackToMainPage.Hide();
                }
                else
                {
                    //ExitProgram.Show();
                    //urlInput.Show();
                    //Go.Show();
                    //BackToMainPage.Show();

                }
                textBox2.Text = textBox2.Text.Replace("admin.123", "");

            }

            if (textBox2.Text.ToString().Length > 30)
            {
                textBox2.Text = "";
            }
        }

        private void NoRadio_CheckedChanged(object sender, EventArgs e)
        {
            if (NoRadio.Checked)
            {
                urlString = UrlStr.Text;
                autoLogin = YesRadio.Checked == true ? true : false;
                bool res = int.TryParse(RefreshFreq.Text, out refreshFrequency);
                string insertQuery = insertQuery = @"INSERT INTO SphinxLogin (Url, AutoLogin, UserNameID, PasswordID, SubmitButtonID, UserName, Password, RefreshFrequency, IPAddress)
VALUES ('" + urlString + "', '" + autoLogin + "','" + null + "','" + null + "','" + null + "','" + null + "','" + null + "','" + refreshFrequency + "','" + MyIp + "');";
                if (!res)
                {
                    timer3.Stop();
                }
                try
                {
                    using (SqlConnection conn = new SqlConnection(ConnectionStrSQL))
                    {
                        using (SqlCommand comm = new SqlCommand(insertQuery, conn))
                        {
                            conn.Open();
                            comm.ExecuteNonQuery();
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                panel2.Hide();

                if (urlString == null || urlString.Length == 0)
                {
                    MessageBox.Show("Invalid URL");
                }
                else
                {
                    webBrowser1.Navigate(urlString);
                }

                if (res && refreshFrequency != 0)
                {
                    timer3.Start();
                    timer3.Interval = refreshFrequency;
                }
                else
                {
                    timer3.Stop();
                }
            }
        }


        private async void RetrievalConfirm_Click(object sender, EventArgs e)
        {
            int ID;
            bool res = int.TryParse(RetrieveIDInput.Text, out ID);
            if (!res)
            {
                //MessageBox.Show("Please input a valid number");
            }
            using (var connection = new SqlConnection(ConnectionStrSQL))
            {
                connection.Open();
                using (var cmd = new SqlCommand("SELECT * FROM SphinxLogin WHERE ID=" + ID + ";", connection))
                {
                    SqlDataReader re = cmd.ExecuteReader();
                    //string urlString;
                    //bool autoLogin;
                    //string usernameID;
                    //string passwordID;
                    //string submitID;
                    //string loginUsername;
                    //string loginPassword;
                    //int refreshFrequency;
                    if (re.Read())
                    {
                        urlString = re["Url"].ToString(); // only fills using first product in table
                        autoLogin = (bool)re["AutoLogin"];
                        usernameID = re["UserNameID"].ToString();
                        passwordID = re["PasswordID"].ToString();
                        submitID = re["SubmitButtonID"].ToString();
                        loginUsername = re["UserName"].ToString();
                        loginPassword = Crypter.Decrypt(re["Password"].ToString());
                        res = int.TryParse(re["RefreshFrequency"].ToString(), out refreshFrequency);
                        var hi = refreshFrequency;
                         if (!res)
                        {
                            timer3.Stop();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Please enter a valid item barcode");
                    }
                }
            }
            if (urlString == null || urlString.Length == 0)
            {
                MessageBox.Show("Invalid URL");
            } else
            {
                webBrowser1.Navigate(urlString);
                // await PageLoad(10000);
                //await for page to load, timeout 10 seconds.
                //your code will run after the page loaded or timeout.
                //while (webBrowser1.IsBusy)
                //    System.Windows.Forms.Application.DoEvents();
                for (int i = 0; i < 500; i++)
                    if (webBrowser1.ReadyState != System.Windows.Forms.WebBrowserReadyState.Complete)
                    {
                        System.Windows.Forms.Application.DoEvents();
                        System.Threading.Thread.Sleep(10);
                    }
                    else
                        break;
                System.Windows.Forms.Application.DoEvents();
                try
                {

                    HtmlDocument doc = webBrowser1.Document;
                    HtmlElement username = doc.GetElementById(usernameID);
                    HtmlElement password = doc.GetElementById(passwordID);
                    HtmlElement submit = doc.GetElementById(submitID);
                    username.SetAttribute("value", loginUsername);
                    password.SetAttribute("value", loginPassword);
                    if (submit != null)
                    {
                        submit.InvokeMember("click");
                    }
                }
                catch (Exception err)
                {
                    Console.WriteLine(err.Message);
                }

                if (res && refreshFrequency != 0)
                {
                    timer3.Start();
                    timer3.Interval = refreshFrequency;
                }
                else
                {
                    timer3.Stop();
                }

                panel1.Hide();
                panel2.Hide();
            }
            
            

        }

        private void Confirm_Click(object sender, EventArgs e)
        {
            //string urlString;
            //bool autoLogin;
            //string usernameID;
            //string passwordID;
            //string submitID;
            //string loginUsername;
            //string loginPassword;
            //int refreshFrequency;
            urlString = UrlStr.Text;
            autoLogin = YesRadio.Checked == true ? true : false;
            usernameID = UNID.Text;
            passwordID = PWID.Text;
            submitID = LGID.Text;
            loginUsername = LoginUN.Text;
            loginPassword = Crypter.Encrypt(LoginPW.Text);
            bool res = int.TryParse(RefreshFreq.Text, out refreshFrequency);
            string insertQuery = insertQuery = @"INSERT INTO SphinxLogin (Url, AutoLogin, UserNameID, PasswordID, SubmitButtonID, UserName, Password, RefreshFrequency, IPAddress)
VALUES ('" + urlString + "', '" + autoLogin + "','" + usernameID + "','" + passwordID + "','" + submitID + "','" + loginUsername + "','" + loginPassword + "','" + refreshFrequency + "','" + MyIp + "');";
            if (!res)
            {
                timer3.Stop();
            }
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStrSQL))
                {
                    using (SqlCommand comm = new SqlCommand(insertQuery, conn))
                    {
                        conn.Open();
                        comm.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            panel2.Hide();
            if (urlString == null || urlString.Length == 0)
            {
                MessageBox.Show("Invalid URL");
            }
            else
            {
                webBrowser1.Navigate(urlString);
            }

            if (res && refreshFrequency != 0)
            {
                timer3.Start();
                timer3.Interval = refreshFrequency;
                loginPassword = Crypter.Decrypt(loginPassword);
                HtmlDocument doc = webBrowser1.Document;
                HtmlElement username = doc.GetElementById(usernameID);
                HtmlElement password = doc.GetElementById(passwordID);
                HtmlElement submit = doc.GetElementById(submitID);
                username.SetAttribute("value", loginUsername);
                password.SetAttribute("value", loginPassword);
                if (submit != null)
                {
                    submit.InvokeMember("click");
                }
                
                panel2.Hide();
            }
            else
            {
                timer3.Stop();
            }
            

        }

     

        private void timer3_Tick(object sender, EventArgs e)
        {
            //window.onbeforeunload = null;
            //string javascript = "window.onbeforeunload = function () {alert('hi');return;}";
            //webBrowser1.Document.InvokeScript("eval", new object[] { javascript });


            //webBrowser1.Navigate(urlString); 


            //SendKeys.Send("^{r}");
            //int ID;
            //bool res = int.TryParse(RetrieveIDInput.Text, out ID);
            //if (!res)
            //{
            //    MessageBox.Show("Please input a valid number");
            //}
            bool res = false;
            using (var connection = new SqlConnection(ConnectionStrSQL))
            {
                connection.Open();
                using (var cmd = new SqlCommand("SELECT * FROM SphinxLogin where IPAddress = '" + MyIp + "';", connection))
                {
                    
                    SqlDataReader re = cmd.ExecuteReader();
                    //string urlString;
                    //bool autoLogin;
                    //string usernameID;
                    //string passwordID;
                    //string submitID;
                    //string loginUsername;
                    //string loginPassword;
                    //int refreshFrequency;
                    if (re.Read())
                    {
                        
                        urlString = re["Url"].ToString(); // only fills using first product in table
                        autoLogin = (bool)re["AutoLogin"];
                        usernameID = re["UserNameID"].ToString();
                        passwordID = re["PasswordID"].ToString();
                        submitID = re["SubmitButtonID"].ToString();
                        loginUsername = re["UserName"].ToString();
                        loginPassword = Crypter.Decrypt(re["Password"].ToString());
                        res = int.TryParse(re["RefreshFrequency"].ToString(), out refreshFrequency);
                        if (!res)
                        {
                            timer3.Stop();
                        }
                    }
                   
                }
            }
            if (urlString == null || urlString.Length == 0)
            {
                MessageBox.Show("Invalid URL");
            }
            else
            {
                webBrowser1.Dispose();
                webBrowser1 = new WebBrowser();
                webBrowser1.Dock = DockStyle.Fill;
                webBrowser1.ScriptErrorsSuppressed = true;
                this.Controls.Add(webBrowser1);
                webBrowser1.Navigate(urlString);
                //waitWebBrowserToComplete(webBrowser1);
                //webBrowser1.DocumentCompleted += (o, w) =>
                //{
                //    webbrowserDocumentCompleted = true;
                //};
                //webBrowser1.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(webBrowser1_DocumentCompleted);

                //await PageLoad(10000);
                //await for page to load, timeout 10 seconds.
                //your code will run after the page loaded or timeout.
                while (webBrowser1.IsBusy)
                    System.Windows.Forms.Application.DoEvents();
                for (int i = 0; i < 500; i++)
                    if (webBrowser1.ReadyState != System.Windows.Forms.WebBrowserReadyState.Complete)
                    {
                        System.Windows.Forms.Application.DoEvents();
                        System.Threading.Thread.Sleep(10);
                    }
                    else
                        break;
                System.Windows.Forms.Application.DoEvents();
                //while ((webBrowser1.IsBusy) | webBrowser1.ReadyState != WebBrowserReadyState.Complete)
                //{
                //    Application.DoEvents();
                //    System.Threading.Thread.Sleep(10);
                //}
                try
                {

                    HtmlDocument doc = webBrowser1.Document;
                    HtmlElement username = doc.GetElementById(usernameID);
                    HtmlElement password = doc.GetElementById(passwordID);
                    //waitWebBrowserToComplete(webBrowser1);
                    HtmlElement submit = doc.GetElementById(submitID);
                    waitWebBrowserToComplete(webBrowser1);
                    username.SetAttribute("value", loginUsername);
                    password.SetAttribute("value", loginPassword);
                    if (submit != null)
                    {
                        submit.InvokeMember("click");
                    }
                }
                catch (Exception err)
                {
                    Console.WriteLine(err.Message);
                }
                
                
                
                

                panel2.Hide();
                //if (res && refreshFrequency != 0)
                //{
                //    timer3.Start();
                   
                //    timer3.Interval = refreshFrequency;

                //}
                //else
                //{
                //    timer3.Stop();
                //}

            }

        }

        bool isConfirm = false;
        //private void Confirm_Click(object sender, EventArgs e)
        //{
        //    isConfirm = true;
        //    url = "https://" + AppWebAddress.Text;
        //    AppWebAddress.Text = "";
        //    if (isConfirm && url != null && url != "https://")
        //    {
        //        DataSet ds = new DataSet();
        //        MessageBox.Show(url);
        //        string query = @"insert into AppWebAddress (Address, Name) values ('" + url + "','" + BtnNum.ToString() + "')";
        //        using (con = new OleDbConnection(ConnectionStrSQL))
        //        using (cmdUpdate = new OleDbCommand(query, con))
        //        using (dad = new OleDbDataAdapter(cmdUpdate))
        //        {
        //            dad.Fill(ds);
        //            MessageBox.Show(url);
        //        }
        //    }

        //}

    

        private void AppWebAddress_TextChanged(object sender, EventArgs e)
        {

        }

      
        //private void button1_Click(object sender, EventArgs e)
        //{
        //    //Button AppBtn = new Button();
        //    //AppBtn.Size = new Size(150, 150);
        //    //AppBtn.Dock = DockStyle.Fill;
        //    AppBtn.Add(new Button());
        //    AppBtn[AppBtn.Count - 1].Name = BtnNum.ToString();
        //    AppBtn[AppBtn.Count - 1].Size = new Size(150, 150);
        //    AppBtn[AppBtn.Count - 1].Dock = DockStyle.Fill;
        //    //tableLayoutPanel1.Controls.Add(new Label { Text = "Type:", Anchor = AnchorStyles.Left, AutoSize = true }, 0, 0);
        //    if (AppNum % 2 == 0)
        //    {
        //        tableLayoutPanel1.Controls.Add(AppBtn[BtnNum], 0, 2);

        //    }
        //    else
        //    {
        //        tableLayoutPanel1.Controls.Add(AppBtn[BtnNum], 1, 2);
        //        tableLayoutPanel1.RowCount++;

        //    }
        //    AppNum++;
        //    if (AppWebAddress.Text == null || AppWebAddress.Text.Length == 0 || AppWebAddress.Text == "https://")
        //    {
        //        MessageBox.Show("Please input an url for this app in the textbox");
        //    }

        //    AppBtn[AppBtn.Count - 1].Click += new EventHandler(AppBtn_Click);
        //    BtnNum++;
        //    isConfirm = false;
        //    //private void AppBtn_Click()
        //    //{
        //    //    string url = "http://www.google.com";
        //    //    webBrowser1.Url = new System.Uri(url);
        //    //}

        //    //if (isConfirm)
        //    //{
        //    //    isConfirm = false;
        //    //    AppWebAddress.Text = null;
        //    //    if (AppWebAddress == "http://" || url == "")
        //    //    {
        //    //        webBrowser1.Url = new System.Uri("http://aurntpas/Newsletter/Newsletter/LatestVolume?Sphinx=True");
        //    //    }
        //    //    else
        //    //    {
        //    //        webBrowser1.Url = new System.Uri(url);
        //    //    }
        //    //}

        //}

        //private void AppBtn_Click(object sender, EventArgs e)
        //{

        //    Button btn = sender as Button;
        //    HideMainPageIcon();
        //    DataSet ds = new DataSet();
        //    string query = "select Address from AppWebAddress where" + btn.Name + "=" + BtnNum.ToString();
        //    using (con = new OleDbConnection(ConnectionStrSQL))
        //    using (cmdUpdate = new OleDbCommand(query, con))
        //    using (dad = new OleDbDataAdapter(cmdUpdate))
        //    {
        //        dad.Fill(ds);
        //    }
        //    DataTable dt = ds.Tables[0];
        //    url = dt.Rows[0]["AppWebAddress"].ToString();
        //    HideMainPageIcon();
        //    webBrowser1.Url = new System.Uri(url);
        //    webBrowser1.Show();

        //}

        //private void Continued_Click(object sender, EventArgs e)
        //{
        //    HideMainPageIcon();
        //}

        //private void Support_Click(object sender, EventArgs e)
        //{
        //    HideMainPageIcon();
        //    webBrowser1.Url = new System.Uri("http://aurnt803/SphinxFeedbacks");
        //    webBrowser1.Show();
        //}

        //private void EventSchedule_Click(object sender, EventArgs e)
        //{
        //    HideMainPageIcon();
        //    webBrowser1.Url = new System.Uri("http://aurntpas/EventScheduler");
        //    webBrowser1.Show();
        //}



        //private void Newsletter_Click(object sender, EventArgs e)
        //{
        //    webBrowser1.Url = new System.Uri("http://aurntpas/Newsletter/Newsletter/LatestVolume?Sphinx=True");
        //    webBrowser1.Show();
        //    HideMainPageIcon();
        //}

        //private void timer2_Tick(object sender, EventArgs e)
        //{
        //    DialogResult dr = MessageBox.Show("Are you still here?", "", MessageBoxButtons.YesNo);
        //    switch (dr)
        //    {
        //        case DialogResult.Yes:
        //            //timer1.Interval = 3000;
        //            timer1.Enabled = false;
        //            break;
        //        case DialogResult.No:
        //            timer1.Enabled = true;
        //            timer2.Enabled = false;
        //            break;
        //        default:
        //            timer1.Enabled = true;
        //            timer2.Enabled = false;
        //            break;
        //    }
        //}

      

        private void homePageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SendKeys.Send("^{r}");
            //webBrowser1.Refresh();
            //if (NewletterIP)
            //{
            //    //webBrowser1.Url = new System.Uri("http://aurntpas/Newsletter/Newsletter/LatestVolume?Sphinx=True");
            //webBrowser1.Url = new System.Uri("https://bipreporting.flextronics.com/BOE/OpenDocument/opendoc/openDocument.jsp?sIDType=CUID&iDocID=AcCUQsqE6pxIjgB1srhXgs0");
            //    webBrowser1.Show();


            //}
            //else
            //{
            //    //webBrowser1.Url = new System.Uri("https://flextronics365.sharepoint.com/sites/business_technology_solutions_applications/SitePages/Home.aspx");
            //    webBrowser1.Url = new System.Uri("https://bipreporting.flextronics.com/BOE/OpenDocument/opendoc/openDocument.jsp?sIDType=CUID&iDocID=AcCUQsqE6pxIjgB1srhXgs0");
            //    webBrowser1.Show(); 
            //    //ShowMainPageIcon();
            //    //webBrowser1.Hide();
            //}

        }



        internal static int GET_POINTER_ID(IntPtr wParam)
        {
            return LOWORD(wParam);
        }
        [DllImport("user32.dll", SetLastError = true)]
        internal static extern bool GetPointerInfo(int pointerID, ref POINTER_INFO pointerInfo);

        public bool PreFilterMessage(ref Message m)
        {
            // Filter out WM_NCRBUTTONDOWN/UP/DBLCLK
            if (m.Msg == 0xA4 || m.Msg == 0xA5 || m.Msg == 0xA6) return true;
            // Filter out WM_RBUTTONDOWN/UP/DBLCLK
            if (m.Msg == 0x204 || m.Msg == 0x205 || m.Msg == 0x206) return true;
            //return false;
            switch (m.Msg)
            {
                case WM_POINTERDOWN:
                case WM_POINTERUP:
                case WM_POINTERUPDATE:
                case WM_POINTERCAPTURECHANGED:
                    int pointerID = GET_POINTER_ID(m.WParam);
                    POINTER_INFO pi = new POINTER_INFO();
                    if (GetPointerInfo(pointerID, ref pi))
                    {
                        // Not a primary pointer => filter !
                        if ((pi.PointerFlags & POINTER_FLAGS.PRIMARY) == 0)
                        {
                            return true;
                        }
                    }
                    break;
            }
            return false;
        }


        
        public void waitWebBrowserToComplete(WebBrowser wb)
        {
            while (!webbrowserDocumentCompleted)
            {
                Application.DoEvents();
            }
            webbrowserDocumentCompleted = false;
        }

       
        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
           
            if (!is_sec_page)
            {
                //if (webBrowser1.ReadyState == WebBrowserReadyState.Complete)
                //if (e.Url.AbsolutePath != (sender as WebBrowser).Url.AbsolutePath)
                //{
                
                try
                {
                   
                    //https://bipreporting.flextronics.com/BOE/OpenDocument/opendoc/openDocument.jsp?sIDType=CUID&iDocID=AcCUQsqE6pxIjgB1srhXgs0
                    //HtmlDocument doc = webBrowser1.Document;
                    //HtmlElement username = doc.GetElementById("_id0:logon:USERNAME");
                    //HtmlElement password = doc.GetElementById("_id0:logon:PASSWORD");
                    //HtmlElement submit = doc.GetElementById("_id0:logon:logonButton");
                    //username.SetAttribute("value", "aurdsata");
                    //password.SetAttribute("value", "Shivviv$4");
                    //submit.InvokeMember("click");
                   
                    HtmlDocument doc = webBrowser1.Document;
                    HtmlElement username = doc.GetElementById(usernameID);
                    HtmlElement password = doc.GetElementById(passwordID);
                    HtmlElement submit = doc.GetElementById(submitID);
                    username.SetAttribute("value", loginUsername);
                    password.SetAttribute("value", loginPassword);
                    submit.InvokeMember("click");
                  

                }
                catch (Exception ex)
                {
                    Console.Write(ex.Message);
                }

                //}
            }
        }
        


        public string GetIP()
        {
            //10.159.165.79
            string localIP;
            using (Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, 0))
            {
                socket.Connect("8.8.8.8", 65530);
                IPEndPoint endPoint = socket.LocalEndPoint as IPEndPoint;
                localIP = endPoint.Address.ToString();
            }
            return localIP;
        }

        private async Task PageLoad(int TimeOut)
        {
            TaskCompletionSource<bool> PageLoaded = null;
            PageLoaded = new TaskCompletionSource<bool>();
            int TimeElapsed = 0;
            webBrowser1.DocumentCompleted += (s, e) =>
            {
                if (webBrowser1.ReadyState != WebBrowserReadyState.Complete) return;
                if (PageLoaded.Task.IsCompleted) return; PageLoaded.SetResult(true);
            };
            //
            while (PageLoaded.Task.Status != TaskStatus.RanToCompletion)
            {
                await Task.Delay(10);//interval of 10 ms worked good for me
                TimeElapsed++;
                if (TimeElapsed >= TimeOut * 100) PageLoaded.TrySetResult(true);
            }
        }

        //private void HideMainPageIcon()
        //{
        //    tableLayoutPanel1.Hide();
        //    Newsletter.Hide();
        //    EventSchedule.Hide();
        //    Support.Hide();
        //    Continued.Hide();

        //}
        //private void ShowMainPageIcon()
        //{
        //    tableLayoutPanel1.Show();
        //    Newsletter.Show();
        //    EventSchedule.Show();
        //    Support.Show();
        //    Continued.Show();
        //}
    }

   


}
